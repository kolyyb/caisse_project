CREATE TABLE `clients` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `email` varchar(255),
  `name` varchar(255),
  `phone` varchar(255),
  `default_adress` integer
);

CREATE TABLE `address` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `street` varchar(255),
  `city` varchar(255),
  `zipcode` varchar(255),
  `state` varchar(255),
  `country` varchar(255)
);

CREATE TABLE `client_address` (
  `client_id` integer,
  `address_id` integer
);

CREATE TABLE `orders` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `order_client_id` integer,
  `delivery_adsress` integer,
  `billing_address` integer,
  `payment_method` integer,
  `order_state` integer,
  `order_line` integer
);

CREATE TABLE `products` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `description` text COMMENT 'Detailed sheet',
  `images` text,
  `price` float,
  `available` boolean
);

CREATE TABLE `order_line` (
  `product_id` integer,
  `order_id` integer,
  `qantity` integer
);

CREATE TABLE `payment_method` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `payment` ENUM ('credit_card', 'bank_check_on_delivery', 'cash_on_delivery')
);

CREATE TABLE `order_state` (
  `id` integer PRIMARY KEY AUTO_INCREMENT,
  `state` ENUM ('cart', 'validated', 'sent', 'delivered')
);

ALTER TABLE `order_line` ADD FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

ALTER TABLE `orders` ADD FOREIGN KEY (`billing_address`) REFERENCES `address` (`id`);

ALTER TABLE `orders` ADD FOREIGN KEY (`delivery_adsress`) REFERENCES `address` (`id`);

ALTER TABLE `client_address` ADD FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

ALTER TABLE `client_address` ADD FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);

ALTER TABLE `orders` ADD FOREIGN KEY (`order_state`) REFERENCES `order_state` (`id`);

ALTER TABLE `orders` ADD FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`);
