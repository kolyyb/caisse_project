## Décrire (à l'écrit) chaque table et ses relations avec les autres tables.

# Table client
* relations : 
plusieurs commandes 
plusieurs adresses
table de jonction entre client et et adresses : clients_address car relation n à n entre client et adress

# Table order
* relations :
plusieurs clients
2 adresses differentes
1 method_payment
plusieurs prduits
1 etat

# Table products
* relations :
plusieurs orders
table relationnelle entre orders et products : order_line n 

# Address
* relations :
plusieurs clients
plusieurs orders

# Payment_method
* relations :
plusieurs orders

# order_state
* relations :
plusieurs orders

# order_line
* relations :
plusieurs orders

## Quelle est la particularité de la relation entre Client et Adress ? A quoi peut elle servir ? Qu'est-ce qu'il y a dans default_adress ?
# Quelle est la particularité de la relation entre Client et Adress ?
Il y a une table specifique client_adresss entre les deux qui contient la clé client et la clé adress repris de la table client.
Un client peut posséder plusieurs adresses: (table Adress) et avoir une adresse par defaut (client.default_adress)

# A quoi peut elle servir ?
On peut recuperer l'adresse dans la table orders (champ delivery_address, billing_address)
mais en utilisant une table de jonction (client_address) on gagne du temps

# Qu'est-ce qu'il y a dans default_adress ?
il y a la paire client_id / address_id de la table client_adress ou la billing_address de la table orders









